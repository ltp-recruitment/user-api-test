# LTPlabs - User API

- The test consists in a simple REST Api with the following requirements:
    - It should be able to create a user
    - It should be able to fetch a user with a URL parameter


- Nice to have:
    - If you have completed the test earlier, you could refactor the create api to allow also an update of a user
    - Unit tests would be a plus

The tools are entirely up to you, you are free to choose whatever tool you feel more confortable but doing it in Django Rest Framework would make a big difference in the final decision.
If the test has been made outside of the premises of LTPlabs Offices, you should put your code as well as the instructions to run the code in a git repository and send us the link to access it.